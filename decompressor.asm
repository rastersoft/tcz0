; This is the TCZ0 decompression functions.
; The first one, 'uncompress_sentence_in_buffer', will decompress a sentence
; in a buffer.
; The second one, 'uncompress_sentende_on_the_fly', will call a function with
; each decompressed character. This is useful if you can operate with the data
; in real time, thus saving the time and the memory space of decompressing it
; in memory first.






; Uncompress a sentence into a buffer.
;
; HL contains the address to the compressed sentence.
; DE contains the address to the buffer where to uncompress the sentence
;
; There must be a label, called "sentence_list", located at the beginning
; of the compressed block.
;
; The entry point is 'uncompress_sentence_in_buffer', but you must copy the code
; from 'uncompress_sentence_in_buffer1'.
;
; Modified registers: AF, BC, DE, HL

uncompress_sentence_in_buffer1:
    inc de
uncompress_sentence_in_buffer:
    ; this is the entry point
    ld a, (hl)
    inc hl
    ld (de), a
    and a
    ret z
    jp p, uncompress_sentence_in_buffer1
do_uncompress_in_buffer:
    ld b, a ; first byte of the address relative to the beginning of the compressed block
    ld c, (hl) ; first byte of the address relative to the beginning of the compressed block
    inc hl
    push hl
    ld l, c
    and 0x0F
    ld h, a ; hl has the offsett from the beginning of the compressed block
    push de
    ld de, sentence_list ; sentence_list must be the beginning of the compressed block
    add hl, de ; now we have in HL the absolute address of the decompressed piece
    pop de
    ld a, b
    rrca
    rrca
    rrca
    rrca
    and 0x07
    add a, 3
    ld b, a ; length
do_uncompress_buffer_loop:
    ld a, (hl)
    ld (de), a
    and a
    jr z, do_uncompress_buffer_ret
    inc hl
    inc de
    djnz do_uncompress_buffer_loop
    pop hl
    jp uncompress_sentence_in_buffer
do_uncompress_buffer_ret:
    pop hl
    ret






; Uncompress a sentence on the fly.
;
; HL contains the address to the compressed sentence.
; It will call the function PROCESS_CHARACTER (set in two CALL instructions).
; You can rename that to whatever you want. This function receives the
; decompressed character in the register A, and it must not modify it, unless
; it want to finalize the decompression in that point, in which case it must
; set A to zero. That will abort the decompression.
; That function also must not modify B, H or L, but can modify F.
;
; It is guaranteed that the function will receive the final ZERO, indicating the
; end of the decompressed sentence.
;
; There must be a label, called "sentence_list", located at the beginning
; of the compressed block.
;
;
; Modified registers: AF, BC, HL

uncompress_sentence_on_the_fly1:
    call PROCESS_CHARACTER
    and a
    ret z

uncompress_sentence_on_the_fly:
    ; this is the entry point
    ld a, (hl)
    inc hl
    and a
    jp p, uncompress_sentence_on_the_fly1
do_uncompress_on_the_fly:
    ld b, a ; first byte of the address relative to the beginning of the compressed block
    ld c, (hl) ; first byte of the address relative to the beginning of the compressed block
    inc hl
    push hl
    ld l, c
    and 0x0F
    ld h, a ; hl has the offsett from the beginning of the compressed block
    push de
    ld de, sentence_list ; sentence_list must be the beginning of the compressed block
    add hl, de ; now we have in HL the absolute address of the decompressed piece
    pop de
    ld a, b
    rrca
    rrca
    rrca
    rrca
    and 0x07
    add a, 3
    ld b, a ; length
do_uncompress_on_the_fly_loop:
    ld a, (hl)
    call PROCESS_CHARACTER
    and a
    jr z, do_uncompress_on_the_fly_ret
    inc hl
    djnz do_uncompress_on_the_fly_loop
    pop hl
    jp uncompress_sentence_on_the_fly
do_uncompress_on_the_fly_ret:
    pop hl
    ret

