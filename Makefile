all: sentences_compressed.asm sentences_english.asm decompressor.asm generate_tester.py loader.tap
	@./generate_tester.py sentences_english.asm sentences_compressed.asm
	@pasmo --tap tester.asm tester.tap
	@cat loader.tap tester.tap > output.tap
	@rm loader.tap tester.tap


# compile the BASIC loader

loader.tap: bas2tap loader.bas
	@./bas2tap -a1 -sTEST loader.bas loader.tap


bas2tap: bas2tap.c
	@gcc -o bas2tap bas2tap.c -lm

clean:
	rm -f *.bin *.z80 *.tap bas2tap tester.asm
