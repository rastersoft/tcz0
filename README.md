# TCZ0, a text decompressor for Z80

TCZ0 is a text decompressor for Z80. It is designed to allow to compress
individual sentences and decompress them at high speed and without needing
intermediate buffers.

The base is LZSS. This compression algorithm stores pairs (length,offset) to
compress repeated blocks. TCZ0 takes advantage of the fact that ASCII is 7 bits
long, thus any character with the bit 7 unset will be considered as uncompressed
text, but if a byte has the bit 7 set, then it and the next one forms a compressed
block. The bits 0-11 form the offset (0 to 4095), and the bits 12-14 contains the
size minus three (thus the length can be from 3 to 10). Every time a compressed
block is found, the decompressor will copy as many bytes as specified by the
length starting from the point specified by the offset.

When a byte with a zero value is found, the decompressor ends. Thus, zero is the
end-of-sentence mark.

The compressor is written in python, and uses simulated annealing to optimize the
compression. This is because the order in which the sentences are feed to the
compressor greatly affects the compression level. Thus, the compressor takes the
list with the sentences, compress it, swaps two sentences, try again, and if the
compression in the new order is better, will keep it; but if it is lower, it will
revert to the previous order and try again. Anyway, to avoid local maximum, it
is possible with a low probability to keep an order that is worse than the
previous; but the program will always keep apart the best compression achieved,
and will store that in disk when the program is stopped.

It is paramount to use pypy3 instead of regular python, because the speed is
much higher.

When the program is launched, it will run forever, trying to squeeze up to the
last byte. Of course, it can happen that it get stuck in a local maximum, so
pressing Ctrl+C will revert to the original order and try again, this time
following a different optimization path (but the best optimization will be
kept anyway, so if the new path is worse and you decide to quit, the good path
will be stored in disk).

To exit the compressor, press Ctrl+C twice.

## Compressing texts

The compressor is *compress_sentences.py*, and receives two parameters: first,
the file with the text to compress, and second, the output file. If the output
file ends in *.asm*, it will generate an assembly file, ready to be used with
the assembly code contained in *decompressor.asm*. But if the file ends in *.c*,
it will generate both *.c* and *.h* files, ready to be used with the C code in
*decompressor.c*.

To use the compressor, you must write all your text in a .asm file.
Each text block must have a label before, because that is what you will need to
uncompress it.

An example, extracted from Escape from M.O.N.J.A.S.

        sentence_intro1:
            DEFB "Personal log: a new promising\n"
            DEFB "day of mission in space begins.",0
        sentence_intro2:
            DEFB "What new adventures will I\n"
            DEFB "encounter today? A battle\n"
            DEFB "against some ferocious\n"
            DEFB "life form?",0
        sentence_intro3:
            DEFB "Rescuing a spaceship?\n"
            DEFB "Or maybe...?",0
        sentence_intro4:
            DEFB "You, dork! The toilet won't\n"
            DEFB "clean itself!",0

Here we can see four text blocks, each one with its own label. If we pass this
through the compressor, it will write in the output file:

        sentence_intro1:
            DEFB "Personal log: a new promising\nday of ",128,23,"sion in space begins.",0
        sentence_intro2:
            DEFB "What",160,15,"adventures will I\nencounter to",128,30,"? A battle\naga",128,56,"t some ferocious\nlif",128,120,"orm?",0
        sentence_intro3:
            DEFB "Rescu",128,26," a",176,46,"ship?\nOr maybe...?",0
        sentence_intro4:
            DEFB "You, dork! The",128,94,"ilet won't\nclea",128,42,"tself!",0

Or, in case that we choose to generate *.c* files, we will have the code file with:

        char sentences_global_list[] = {
            'P', 'e', 'r', 's', 'o', 'n', 'a', 'l', ' ', 'l', 'o', 'g', ':', ' ', 'a', ' ', 'n', 'e', 'w', ' ', 'p', 'r', 'o', 'm', 'i', 's', 'i', 'n', 'g', 10, 'd', 'a', 'y', ' ', 'o', 'f', ' ', 128, 23, 's', 'i', 'o', 'n', ' ', 'i', 'n', ' ', 's', 'p', 'a', 'c', 'e', ' ', 'b', 'e', 'g', 'i', 'n', 's', '.', 0,
            'W', 'h', 'a', 't', 160, 15, 'a', 'd', 'v', 'e', 'n', 't', 'u', 'r', 'e', 's', ' ', 'w', 'i', 'l', 'l', ' ', 'I', 10, 'e', 'n', 'c', 'o', 'u', 'n', 't', 'e', 'r', ' ', 't', 'o', 128, 30, '?', ' ', 'A', ' ', 'b', 'a', 't', 't', 'l', 'e', 10, 'a', 'g', 'a', 128, '8', 't', ' ', 's', 'o', 'm', 'e', ' ', 'f', 'e', 'r', 'o', 'c', 'i', 'o', 'u', 's', 10, 'l', 'i', 'f', 128, 'x', 'o', 'r', 'm', '?', 0,
            'R', 'e', 's', 'c', 'u', 128, 26, ' ', 'a', 176, '.', 's', 'h', 'i', 'p', '?', 10, 'O', 'r', ' ', 'm', 'a', 'y', 'b', 'e', '.', '.', '.', '?', 0,
            'Y', 'o', 'u', ',', ' ', 'd', 'o', 'r', 'k', '!', ' ', 'T', 'h', 'e', 128, '^', 'i', 'l', 'e', 't', ' ', 'w', 'o', 'n', '\'', 't', 10, 'c', 'l', 'e', 'a', 128, '*', 't', 's', 'e', 'l', 'f', '!', 0,
        };

and the header file with:

        /* This file was generated automatically
        Do not modify; instead, run compress_sentences.py again */

        #ifndef TCZ0_SENTENCES
        #define TCZ0_SENTENCES

        extern char sentences_global_list[];

        #define SENTENCE_sentence_intro1 0
        #define SENTENCE_sentence_intro2 61
        #define SENTENCE_sentence_intro3 142
        #define SENTENCE_sentence_intro4 172

        #endif // TCZ0_SENTENCES

As you can see, each compressed sentence keeps the label, but now the data is
compressed. But is important to NOT reorder this output file, because the offsets
in each sentence are relative to the beginning of the whole file, thus changing
anything in this file would break the compressed text.

Of course, this example is quite short and can't be compressed a lot, but with
bigger lengths, the final data can be less than the 60% of the original size.

The compressor also will read all the .asm files and check which labels are used,
to ensure that all the texts in this file are used there. All the labels that
aren't used in other .asm files will be shown in screen, to allow the programmer
to remove them and save space by not adding texts that won't be used.

While running you will see the maximum compression ratio in the current path,
the maximum compression ratio achieved until now, and how many blocks of each
size have been generated. When you press Ctrl+C twice, the final values will
be printed, and also the largest text block found (useful to determine the
size of the buffer needed to decompress all of them).

It's a good idea to leave the compressor working during the night (8 hours)
to squeeze the maximum compression level.

## Uncompressing texts

There are two files available: *decompressor.c* and *decompressor.asm*.

You can use *decompressor.c* with both files (*XXX.c* and *XXX.h*) generated
by *compress_sentences.py*. To uncompress a sentence, just call *decompress()*
passing to it as the first parameter the sentence identifier (it's just the
sentence label prefixed by *SENTENCE_*. You have the whole list in the header
file generated by *compress_sentences.py*), and a pointer to a buffer with
enough size for the sentence plus the ZERO at the end as the second parameter.

There are two functions available in *decompressor.asm*, one to decompress one
text block into a buffer, and another to decompress it *on-the-fly*. The first
is the easiest to use: just put in HL the address of the block to uncompress
(you should use the labels), put in DE the address of the buffer where to store
the uncompressed text block, and do a *CALL uncompress_sentence_in_buffer*.

The decompression on-the-fly requires to write a callback function. This function
will be called every time one character has been decompressed, and that character
will be passed in A. It is mandatory that the function doesn't modify A, HL or BC.
The function will preserve DE, and doesn't use IX, IY or any of the alternate
registers, so they can be used freely inside the callback function or to pass
parameters to it (an example: DE can be used to contain the current screen
coordinates for printing the characters).

By default, the callback function should be called *PROCESS_CHARACTER*, but
you can rename it in the decompressing function (remember that there are
TWO appearances).

To call the on-the-fly version, just load HL with the address of the text block
to decompress, and call it.

Since the decompressor needs to have a reference to the beginning of the whole
compressed sentences list. For this, it uses the label *sentence_list*, and the
compressor already adds it at the beginning of the output file.

It is important to take into account that both in the buffered and the callback
versions, the final ZERO byte will be stored in the buffer/passed to the callback.

## Checking code

There is a file called *generate_tester.py* which receives as parameters the
uncompressed and the compressed files, and will use both *to_buffer* and *on-the-fly*
functions to ensure that everything works fine. This is useful to do tests if
you decide to change the decompression functions.

The program generates a .tap file that can be run in a Sinclair Spectrum. If
everything goes right, it will show a success message; but if any of the decompressed
text block doesn't coincide with the original uncompressed text, it will be shown
on screen.

To do quick tests, you can use *sentences_english.asm* and *sentences_compressed.asm*
files.

## Future

I have some ideas to make the function to compress even more in the future.

* Currently, the compressor gives priority to the longest compressible block. But
in some cases this can be a bad idea, so I want to try all the possible cases
when two or more compressed blocks overlap. This change would be compatible with
the current decompressor.

* Another change is to take advantage of the ASCII control codes to store simple
two-byte-long blocks, using the byte itself as offset. A bigger improvement could
be to take all the character codes used and remap them from 1 onwards, and use
the remaining up to 127 as the offset of two-byte-long blocks. If the program uses
its own character set, it can be rearranged in the same order, thus no translation
table would be needed to convert from the "ordered codes" into "ASCII codes".
Even better: any unused character would be removed from the character set, thus
saving more space. It also would allow to use LATIN1 characters more easily.
Unfortunately, this change would be incompatible with the current decompressor,
so it would probably be a different project.

## Author

Sergio Costas (raster software vigo)  
rastersoft@gmail.com  
https://www.rastersoft.com  
https://gitlab.com/rastersoft/tcz0
