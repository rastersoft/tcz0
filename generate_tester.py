#!/usr/bin/env python3

import sys

labels = []

source_code = f"""

org 32768

    call 0x0DAF ; ROM CLS
    ld hl, testing
    call print_sentence
    jp do_test

all_ok:
    defb "ALL OK", 0;

error_msg:
    defb "  ERROR", 13, 13, 0

testing:
    defb "Testing... ",13,13,0

; HL compressed sentence
; DE original sentence
do_check:
    push de
    ld de, 23296
;    push af
;    call c, do_in_buffer
;    pop af
;    call nc, do_on_the_fly
    call uncompress_sentence_in_buffer
    pop de
    push de
    ld hl, 23296
do_check_loop
    ld a, (de)
    cp (hl)
    jr nz, do_check_error
    and a
    jr z, do_check_return
    inc hl
    inc de
    jr do_check_loop

do_in_buffer:
    ld a, 48
    ld (error_msg), a
    jp uncompress_sentence_in_buffer

do_on_the_fly:
    ld a, 49
    ld (error_msg), a
    jp uncompress_sentence_on_the_fly

do_check_return:
    pop de
    ret

do_check_error:
    ld hl, error_msg
    call print_sentence
    pop hl ; original sentence
    call print_sentence
    ld a, 13
    rst #10
    ld a, 13
    rst #10
    ld hl, 23296
    call print_sentence
    di
    halt

PROCESS_CHARACTER:
    ld (de), a
    inc de
    ret

print_sentence:
    ; receives in HL a sentence to print into screen, ended in 0
    ld a, (hl)
    and a
    ret z
    cp 10
    jr nz, print_sentence2
    ld a, 13
print_sentence2:
    push hl
    rst #10
    pop hl
    inc hl
    jp print_sentence

include "decompressor.asm"

include "{sys.argv[2]}"

"""

with open(sys.argv[1], "r") as input_data:
    for line in input_data.readlines():
        line = line.strip()
        if (len(line) != 0) and (line[-1] == ':'):
            labels.append(line[:-1])
            line = f"original_{line}"
        else:
            line = f"    {line}"
        source_code += (f"{line}\n")


source_code += """

do_test:
"""

counter = 32
for entry in labels:
    counter += 1
    source_code += f"""
    ld hl, {entry}
    ld de, original_{entry}
    and a ; check "on the fly"
    call do_check
    ld hl, {entry}
    ld de, original_{entry}
    scf ; check on buffer
    call do_check
"""

source_code += """
    ld hl, all_ok
    call print_sentence
    ret
"""

with open("tester.asm", "w") as output_data:
    output_data.write(source_code)

